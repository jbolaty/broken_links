#! /usr/local/bin/env python3.6
# -*-unicode: "utf-8" -* 
#------------------------------------------------------------
# Nom: Bolaty
# Prenom : Jean-Marc
# link='http://educ-a-dom.fr/brklnk/'
#-------------------------------------------------------------

import argparse
from bs4 import BeautifulSoup
from urllib import error
from urllib import request
import requests
import re


# ---------------------- ARGPARSE --------------------------------------
parser = argparse.ArgumentParser()
parser.add_argument("url", help=" lien du site ") 
parser.add_argument("-help", help=" affiche l'usage de la commande ") 
parser.add_argument("-depth", help="profondeur de la recherche n (1 par defaut) ", type=int, default=1)
args= parser.parse_args()

# ------------------------------------------------------------- 

def check_for_BrokenLinks(link, depth):

    """ Function used to check recurssively if links on a given web page are broken.
        the depth indicate the level of research (default =1)   """

    if depth > 0:

        # getting the link and try if broken or not 
        response = requests.get(link)
        if response.status_code >= 400:
            print("ERROR : ", response.status_code, " , The following link is broken :\n ", link, "\n")

        else :  
            
            parsing_page = BeautifulSoup(request.urlopen(link), 'html.parser')
            a_anchors = parsing_page.find_all('a') # List of all "a" anchors in the link 
            
            for a_anchor in a_anchors : 
                
                link_on_current_webpage = a_anchor.get('href') # getting the exact adress link in the "a" anchor
                

                # if no protocol, use domain name of parent 
                if not link_on_current_webpage in ['http', 'https', 'ssh']:
                    link_on_current_webpage = link + link_on_current_webpage  
            
                check_for_BrokenLinks(link_on_current_webpage, depth-1) 


# ------------------------------------------------------------- 
if __name__ == "__main__":
    check_for_BrokenLinks(args.url, args.depth)


